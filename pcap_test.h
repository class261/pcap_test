#pragma once

#include <string.h>
#include <netinet/in.h>
#include <pcap.h>
#include <stdbool.h>
#include <stdio.h>

#ifndef ether_mac_len
#define ether_mac_len 6
#endif

#ifndef ether_type_len
#define ether_type_len 2
#endif

#ifndef ip_len
#define ip_len 4
#endif

#ifndef ip_to_tcp
#define ip_to_tcp 6
#endif

#ifndef tcp_len
#define tcp_len 2
#endif

#ifndef data_max_len
#define data_max_len 10
#endif

typedef struct s_Ether_header {
    unsigned char src_mac[ether_mac_len];
    unsigned char dst_mac[ether_mac_len];
    unsigned char ether_type;
} Ether_header;

typedef struct s_IP_header {
    unsigned char IP_ver;
    unsigned char Header_len;
    unsigned char Type;
    unsigned char Packet_len;
    unsigned char Frag_iden;
    unsigned char Frag_flag;
    unsigned char Frag_offset;
    unsigned char TTL;
    unsigned char Protocol_iden;
    unsigned char checksum;

    unsigned char src_ip[ip_len];
    unsigned char dst_ip[ip_len];
} IP_header;

typedef struct s_TCP_header {
    unsigned char src_port[tcp_len];
    unsigned char dst_port[tcp_len];

    unsigned char seq_num;
    unsigned char ACK_num;
    unsigned char offset;
    unsigned char reserved;
    unsigned char flag;
    unsigned char window_size;
    unsigned char checksum;
    unsigned char URG_flag;
    unsigned char option;
} TCPh;

typedef struct s_data_value {
    unsigned char hex_val[data_max_len];
} data_value;

void print_info(const unsigned char *packet);


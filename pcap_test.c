#include <string.h>
#include <netinet/in.h>
#include <pcap.h>
#include <stdbool.h>
#include <stdio.h>

#include "pcap_test.h"

#define ether_to_ip 0x0800



void print_info_ether(char *src, char *dst, const u_char* packet);
void print_info_ip(char *src_ip, char *dst_ip);
void print_info_tcp(char *src_port, char *dst_port);
void print_info_data(int offset, struct pcap_pkthdr* header, const u_char* packet);

int mac_Adr(const unsigned char* packet, char* src_mac, char* des_mac);
int IP_Adr(const unsigned char* packet, char* src_ip, char* des_ip);
int TCP_Adr(const unsigned char* packet, char* src_port, char* des_port);

void usage() {
	printf("syntax: pcap-test <interface>\n");
	printf("sample: pcap-test wlan0\n");
}

typedef struct {
	char* dev_;
} Param;

Param param = {
	.dev_ = NULL
};

bool parse(Param* param, int argc, char* argv[]) {
	if (argc != 2) {
		usage();
		return false;
	}
	param->dev_ = argv[1];
	return true;
}

int main(int argc, char* argv[]) 
{
	if (!parse(&param, argc, argv))
		return -1;

	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_t* pcap = pcap_open_live(param.dev_, BUFSIZ, 1, 1000, errbuf);
	if (pcap == NULL) {
		fprintf(stderr, "pcap_open_live(%s) return null - %s\n", param.dev_, errbuf);
		return -1;
	}

	while (true) 
    {
		struct pcap_pkthdr* header;
		const u_char* packet;

        char src_mac[30] ={0,}; 
		char dst_mac[30] ={0,};
		char src_ip[30] ={0,}; 
		char dst_ip[30] ={0,};
		char *src_port;
		char *dst_port;

		int res = pcap_next_ex(pcap, &header, &packet);
        int ip_offset = 0;
        int tcp_offset = 0;

		if (res == 0) continue;
		if (res == PCAP_ERROR || res == PCAP_ERROR_BREAK) {
			printf("pcap_next_ex return %d(%s)\n", res, pcap_geterr(pcap));
			break;
		}

        if (mac_Adr(packet, src_mac, dst_mac) < 0)
            break;

        print_info_ether(src_mac, dst_mac, packet);

        if (ip_offset = IP_Adr(packet, src_ip, dst_ip) < 0)
            break;

        packet += ip_offset * 4;

        print_info_ip(src_ip, dst_ip);

        tcp_offset = TCP_Adr(packet, src_port, dst_port);

        packet += tcp_offset * 4;

        print_info_tcp(src_port, dst_port);
        print_info_data(ip_offset + tcp_offset + 14, header, packet);

		printf("%u bytes captured\n", header->caplen);
	}

	pcap_close(pcap);
    return (0);
}

void print_info_ether(char *src, char *dst, const u_char* packet)
{
    printf("\n===== Ethernet Header =====\n");
    printf(" Src mac : %s \n", src);
    printf(" Dst mac : %s \n", dst);

    packet += 14;
}

void print_info_ip(char *src_ip, char *dst_ip)
{
    printf("\n======== IP Header ========\n");
    printf(" Src IP : %s\n", src_ip);
    printf(" Dst IP : %s\n", dst_ip);
}

void print_info_tcp(char *src_port, char *dst_port)
{
    printf("\n======== TCP Header ========\n");
    printf(" Src port : %u\n", src_port);
    printf(" Dst port : %u\n", dst_port);
}

void print_info_data(int offset, struct pcap_pkthdr* header, const u_char* packet)
{
    printf("\n========= Payload =========\n");
    printf(" * max 10 bytes\n");
    for (int i = 0; i < header->caplen - offset; i ++)
    {
        if (i == 8) 
            break;
        printf("%02x", packet[i]);
    }
    printf("\n");
}

int mac_Adr(const unsigned char* packet, char* src_mac, char* des_mac)
{
	Ether_header *ether;
	ether = (Ether_header *)packet;

    unsigned char *addr1, *addr2;
	
	if(ntohs(ether->ether_type) != ether_to_ip)
		return (-1);

    addr1 = ether->src_mac;
    addr2 = ether->dst_mac;
	sprintf(src_mac, "%02x:%02x:%02x:%02x:%02x:%02x", addr1);
	sprintf(des_mac, "%02x:%02x:%02x:%02x:%02x:%02x", addr2);
    
    return (0);
}

int IP_Adr(const unsigned char* packet, char* src_ip, char* des_ip)
{
	IP_header *ip;
	ip = (IP_header *)packet;

    unsigned char *addr1, *addr2;
	
	if(ntohs(ip->Protocol_iden) != 0x06)
		return (-1);

    addr1 = ip->src_ip;
    addr2 = ip->dst_ip;
	sprintf(src_ip, "%u.%u.%u.%u", addr1);
	sprintf(des_ip, "%u.%u.%u.%u", addr2);

    return (ip->Header_len);
}

int TCP_Adr(const unsigned char* packet, char* src_port, char* dst_port)
{
	TCPh *tcp;
	tcp = (TCPh *)packet;
	
    unsigned char *addr1, *addr2;

    *src_port = ntohs(tcp->src_port);
    *dst_port = ntohs(tcp->dst_port);

    return (tcp->offset);
}





